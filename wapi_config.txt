{
  "save_as": "csv",
  "log_file_location": "wapiclient.log",
  "credentials_location": "wapi_credentials.txt",
  "curves_input_file": "wapi_input.txt",
  "output_folder": "./out/",
  "mssql_servername": "",
  "mssql_dbname": "",
  "mssql_username": "",
  "mssql_password": "",
  "driver": "ODBC DRIVER 17 for SQL Server",
}