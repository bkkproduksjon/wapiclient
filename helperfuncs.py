import datetime
import time
import os  # needed for saving file
import errno # needed for exception handling in save_ps
import json # read json data
import sys # stderr and exit

# loads config file for wapi client
configfile = 'wapi_config.txt'
try:
    wapiconfig = json.load(open(configfile, 'r'))
except IOError as e:
    sys.stderr.write("File " + configfile + " can't be opened")
    sys.exit(1)

# returns a value from config dict.
# INPUT: string key - a dict key 
def get_config(key):
    return wapiconfig[key]

# reads file containing wattsight API oauth2 credentials and returns them as a dict
def get_credentials():
    credentials_location = get_config("credentials_location")
    try:
        credentials = json.load(open(credentials_location, 'r'))
        writeToLog("Credentials file opened and parsed")
        return credentials
    except IOError:
        writeToLog("Cannot open credentials file.")
        sys.exit(1)

# reads input file and returns an array of dicts, each one describing a wapi curve 
def read_inputf():
    inputfile = get_config("curves_input_file")
    try:
        curves = json.load(open(inputfile))
        writeToLog("Curve input file opened and parsed.")
        return curves
    except IOError:
        writeToLog("Cannot open input file " + inputfile)
        sys.exit(1)

# returns current time as UNIX time stamp as an integer value
def timenow():
    dt = datetime.datetime.now()
    return int(time.mktime(dt.timetuple()))

# returns current date as a string
def dateString():
    return datetime.datetime.today().strftime('%Y-%m-%d')

# returns current date and time as a string
def dateStringTime():
    return datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

# Writes a message string to a log file, location of which is specified in config file
# INPUT: string message
def writeToLog(message):
    filename = wapiconfig["log_file_location"]
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    with open(filename, 'a') as log:
        log.write(dateStringTime() + ": " + message + "\n")
        log.close()

# saves a Panda Series object to a csv file, json file or an SQL database
# depending on config setting "save_as"
# INPUT: Panda Series ps, Wapi Curve object c
def save_ps(ps, c):
    sv = wapiconfig["save_as"]
    name = c.name.replace("/", "_")
    outname = wapiconfig["output_folder"] + name.replace(" ", "-")
    if not os.path.exists(os.path.dirname(outname)):
        try:
            os.makedirs(os.path.dirname(outname))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    if sv == 'csv':
        outname += '.csv'
        ps.to_csv(outname, sep=";", decimal=",")
        writeToLog(c.name + " written to " + outname)
    elif sv == 'json':
        outname += '.json'
        ps.to_json(outname)
        writeToLog(c.name + " written to " + outname)
    #elif sv == 'mssql':
    