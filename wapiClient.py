import wapi
import pandas as pd
import helperfuncs as hf
import json
from requests import exceptions
import urllib3

# An idea: take in an array of issue dates to get multiple prognoses for the same curve, issued at different dates
#strIssued = json.loads('{"issue_date": ["2018-07-19", "2018-07-20"]}')

# Gets curve data, based on curve type and user-provided params
# INPUT: c - a dict containing params read in from input files
#        curve - a curve object fetched from Wattsight
# OUTPUT: a Panda Series object representing the curve
def get_ps(c, curve):
    # check what input is set in input file. Set to defaults if not.
    if not c.get("data_from"):
        c["data_from"] = hf.dateString() # set to current date

    if curve.curve_type == 'INSTANCES':
        # Currently, wapi doesn't support any params but issue date for INSTANCE curves
        # function and frequency, data_from and data_to are not yet implemented in wapi
        if not c.get("issue_date"):
            ts = curve.get_latest()
        else:
            ts = curve.get_instance(issue_date=c["issue_date"])
        ps = ts.to_pandas()

        # use frequency and function param manually here
        if c.get("frequency"):
            if c["function"] == "AVERAGE":
                ps = ps.resample(c["frequency"]).mean()
            elif c["function"] == "SUM":
                ps = ps.resample(c["frequency"]).sum()
        return ps

    elif curve.curve_type == 'TIME_SERIES':
        # see https://api.wattsight.com/#documentation, API: Time Series curve
        allowed_keys = {"data_from", "data_to", "filter", "function", "frequency"} # as defined in wapi
        params = dict(c)
        for k in c:
            if not k in allowed_keys:
                del params[k]

        ts = curve.get_data(**params)
        return ts.to_pandas()

hf.writeToLog("----Started running wapi client.----")

# Open a session using the 'wapi' library with client credentials as arguments
credentials = hf.get_credentials()
try:
    curves = hf.read_inputf()
    session = wapi.Session(**credentials)
    hf.writeToLog("Wattsight session opened (authentication validated).")
    for c in curves:
        try:
            #Curve IDs might change in the future, so get by name instead
            curve = session.get_curve( name = c["name"] )
            # Check if user account has access to curve
            if not curve.access():
                hf.writeToLog("Current Wattsight user doesn't have access to curve '" + curve.name + "'. Contact Wattsight to get access.")
                continue
            else:
                ps = get_ps(c, curve)
                hf.save_ps(ps, curve)
        #except CurveException -- where is this one defined? In wapi?

        except AttributeError as atterr:
            # This error comes up if get_data() or get_instance() in get_ps() return a 'Nonetype' and the next line tries to convert it to a Pandas Series.
            hf.writeToLog("The curve " + c["name"] + "requsted using given input doesn't exist. Set issue date or data_from/_to to blank or search for the correct input at api.wattsight.com")

except exceptions.SSLError as sslerr:
    hf.writeToLog("Opening session failed:" + str(sslerr))
    print("Opening session failed:", sslerr)
    sys.exit(1)

hf.writeToLog("---- WapiClient stopped ----\n")