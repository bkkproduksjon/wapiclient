[
 {
  "name": "con np mwh/h cet min15 af",
  "data_from": "2018-07-18",
  "data_to": "2018-07-19",
  "issue_date": "2018-07-18",
  "frequency": "H",
  "function": "AVERAGE"
 },
 {
  "name": "pro np wnd ec00 mwh/h cet min15 f",
  "issue_date": "2018-07-25",
  "frequency": "H",
  "function": "AVERAGE"
 },
 {
  "name": "pro np nuc mwh/h cet h af",
  "data_from": "2018-07-18",
  "data_to": "2018-07-19",
  "issue_date": "2018-07-18",
  "frequency": "H",
  "function": "AVERAGE"
 },
 {
  "name": "pri de spot ec00 �/mwh cet h f",
  "data_from": "",
  "data_to": "2018-07-19",
  "issue_date": "",
  "frequency": "H",
  "function": "AVERAGE"
 },
 {
  "name": "exc de spot ec00 mwh/h cet h f",
  "data_from": "2018-07-18",
  "data_to": "2018-07-19",
  "issue_date": "",
  "frequency": "H",
  "function": "AVERAGE"
 }
]